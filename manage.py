from flask.ext.script import Manager, Server
from flask.ext.soy import CompileSoyCommand

from muziq.app import app, soy

manager = Manager(app)
manager.add_command("runserver", Server(port=5001, extra_files=soy.find_templates()))
manager.add_command("compile_soy", CompileSoyCommand())

manager.run()


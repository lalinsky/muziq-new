#!/usr/bin/env bash

set -e

solr_version=4.7.1
solr_download_url=http://tux.rainside.sk/apache/lucene/solr/${solr_version}/solr-${solr_version}.tgz

dest=$1

tmp_dir=`mktemp -d`
trap "rm -rf $tmp_dir" EXIT

cd $tmp_dir
wget $solr_download_url
tar xf solr-${solr_version}.tgz

rm -rf $dest
mkdir $dest

for name in contexts etc lib logs resources solr-webapp start.jar webapps
do
    mv solr-$solr_version/example/$name $dest/$name
done

cat >$dest/start.sh <<EOF
#!/bin/sh
cd /opt/solr
exec java -Dsolr.solr.home=/var/lib/solr -jar start.jar
EOF
chmod +x $dest/start.sh

chown solr:solr -R $dest


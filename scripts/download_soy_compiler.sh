#!/usr/bin/env bash

set -e

url=$1
dest=$2

tmp_dir=`mktemp -d`
trap "rm -rf $tmp_dir" EXIT

cd $tmp_dir
wget $url
unzip *.zip
mv *.jar $dest


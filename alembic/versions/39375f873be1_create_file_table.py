"""create file table

Revision ID: 39375f873be1
Revises: 2a2e026cff3f
Create Date: 2014-03-19 20:42:08.727407

"""

# revision identifiers, used by Alembic.
revision = '39375f873be1'
down_revision = '2a2e026cff3f'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('file',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('user_id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('uploaded', sa.DateTime(), nullable=False),
        sa.Column('sha256_original', sa.String(), nullable=False),
        sa.Column('sha256_m4a', sa.String(), nullable=True),
        sa.ForeignKeyConstraint(['user_id'], ['account.id']),
        sa.PrimaryKeyConstraint('id')
    )


def downgrade():
    op.drop_table('file')


"""create add file.deleted

Revision ID: fa986496e0
Revises: 39375f873be1
Create Date: 2014-04-06 10:01:25.613849

"""

# revision identifiers, used by Alembic.
revision = 'fa986496e0'
down_revision = '39375f873be1'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('file', sa.Column('deleted', sa.Boolean(), server_default='false', nullable=False))


def downgrade():
    op.drop_column('file', 'deleted')

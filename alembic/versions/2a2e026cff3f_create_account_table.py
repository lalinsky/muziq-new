"""create account table

Revision ID: 2a2e026cff3f
Revises: None
Create Date: 2014-03-19 20:31:33.797687

"""

# revision identifiers, used by Alembic.
revision = '2a2e026cff3f'
down_revision = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('account',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('email', sa.String(), nullable=False),
        sa.Column('password', sa.String(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.PrimaryKeyConstraint('id'),
    )


def downgrade():
    op.drop_table('account')


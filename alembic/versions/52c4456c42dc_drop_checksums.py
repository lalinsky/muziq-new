"""drop checksums

Revision ID: 52c4456c42dc
Revises: fa986496e0
Create Date: 2014-04-12 20:12:14.092466

"""

# revision identifiers, used by Alembic.
revision = '52c4456c42dc'
down_revision = 'fa986496e0'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.drop_column('file', 'sha256_original')
    op.drop_column('file', 'sha256_m4a')


def downgrade():
    op.add_column('file', sa.Column('sha256_m4a', sa.VARCHAR(), nullable=True))
    op.add_column('file', sa.Column('sha256_original', sa.VARCHAR(), nullable=False))

import sys
import logging
from flask import Flask, g, request, session, redirect, jsonify
from flask.ext.soy import Soy, render_template_ex, render_js_templates
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from muziq.models import User
from muziq.utils import camelize, soy_json

app = Flask(__name__)
app.config.from_object('muziq.commonsettings')
app.config.from_envvar('MUZIQ_SETTINGS')

soy = Soy(app)

engine = create_engine(app.config['DATABASE_URI'], echo=app.config['DATABASE_ECHO'])
Session = sessionmaker(bind=engine)

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)


@app.before_request
def before_request():
    g.db = Session()
    if 'user' in session:
        user_data = session['user']
        g.user = User(
            id=user_data['id'],
            email=user_data['email'],
            name=user_data['name'])
    else:
        g.user = None


@app.teardown_request
def teardown_request(exception):
    if hasattr(g, 'db'):
        g.db.close()


@app.context_processor
def process_context():
    context = {}
    user = session.get('user')
    if user is not None:
        context['user'] = user

    return context


@app.route('/static/js/templates.js')
def templates_js():
    rv = render_js_templates()
    rv.data = '\n'.join([
        'define(["soy"], function (soy) {',
            rv.data,
            'return { muziq: muziq };',
        '});',
    ])
    return rv


def request_wants_json():
    best = request.accept_mimetypes \
        .best_match(['application/json', 'text/html'])
    return best == 'application/json' and \
        request.accept_mimetypes[best] > \
        request.accept_mimetypes['text/html']


def render_json(**context):
    app.update_template_context(context)
    for name in ('g', 'request', 'session'):
        del context[name]
    if 'ok' not in context:
        context['ok'] = True
    return jsonify(**context)


def render_template_or_json(template, **context):
    if request_wants_json():
        return render_json(**context)
    routes = {}
    for rule in app.url_map.iter_rules():
        if rule.endpoint not in ('static', 'templates_js'):
            routes[rule.rule.lstrip('/')] = camelize(rule.endpoint)
    ij_data = {'routesJson': soy_json(routes) }
    return render_template_ex(template, context, ij_data)


import muziq.views.index
import muziq.views.login
import muziq.views.music
import muziq.views.files
import muziq.views.search


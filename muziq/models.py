from sqlalchemy import sql, Column, Integer, String, DateTime, Boolean, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class User(Base):
    __tablename__ = 'account'
    id = Column(Integer, primary_key=True)
    email = Column(String, nullable=False)
    password = Column(String, nullable=False)
    name = Column(String, nullable=False)


class File(Base):
    __tablename__ = 'file'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('account.id'), nullable=False)
    user = relationship('User')
    name = Column(String, nullable=False)
    uploaded = Column(DateTime, nullable=False, default=sql.func.now())
    deleted = Column(Boolean, nullable=False, server_default=sql.text('false'), default=False)


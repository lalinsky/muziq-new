import math
import logging
from flask import g, session, request, redirect, abort, send_file, url_for
from itsdangerous import URLSafeTimedSerializer
from boto.exception import S3ResponseError
from boto.s3.connection import S3Connection
from muziq.app import app, render_template_or_json, render_json, request_wants_json
from muziq.files.upload import store_file
from muziq.models import File

logger = logging.getLogger(__name__)


class Pagination(object):

    def __init__(self, page, per_page, total_count):
        self.page = page
        self.per_page = per_page
        self.total_count = total_count

    @property
    def pages(self):
        return int(math.ceil(self.total_count / float(self.per_page)))

    @property
    def has_prev(self):
        return self.page > 1

    @property
    def has_next(self):
        return self.page < self.pages

    def iter_pages(self, left_edge=2, left_current=2,
                   right_current=5, right_edge=2):
        last = 0
        for num in xrange(1, self.pages + 1):
            if num <= left_edge or \
               (num > self.page - left_current - 1 and \
                num < self.page + right_current) or \
               num > self.pages - right_edge:
                if last + 1 != num:
                    yield None
                yield num
                last = num

    def to_json(self):
        return {
            "page": self.page,
            "perPage": self.per_page,
            "totalCount": self.total_count,
            "pages": self.pages,
            "hasPrev": self.has_prev,
            "hasNext": self.has_next,
            "pageList": list(self.iter_pages())
        }


def export_file(file):
    return {'id': file.id, 'name': file.name, 'uploaded': file.uploaded}


def get_files(db, user_id, limit=10, offset=0):
    files = db.query(File).filter_by(user_id=g.user.id).\
        filter_by(deleted=False).order_by(File.uploaded).\
        limit(limit).offset(offset).all()
    return map(export_file, files)


def get_files_count(db, user_id):
    return db.query(File).filter_by(user_id=g.user.id).\
        filter_by(deleted=False).count()


@app.route('/files')
def files():
    if g.user is None:
        return redirect('/login')
    page = request.args.get('page', type=int, default=1)
    if page < 1:
        page = 1
    per_page = 50
    total_count = get_files_count(g.db, g.user.id)
    files = get_files(g.db, g.user.id, limit=per_page, offset=(page-1)*per_page)
    pagination = Pagination(page, per_page, total_count)
    return render_template_or_json('muziq.filesPage', files=files, pagination=pagination.to_json())


def create_serializer():
    return URLSafeTimedSerializer(app.config['SECRET_KEY'])


@app.route('/files/stream')
def files_stream():
    if g.user is None:
        return redirect('/login')
    file_id = request.args.get('id', type=int)
    s3_file = get_s3_bucket().get_key(str(file_id))
    if s3_file is None:
        return render_json(ok=0)
    stream_url = s3_file.generate_url(60 * 30)
    return render_json(ok=1, streamUrl=stream_url)


def get_s3_bucket():
    s3 = S3Connection(
        app.config['S3_ACCESS_KEY_ID'],
        app.config['S3_SECRET_ACCESS_KEY'],
        host=app.config['S3_HOST'],
        is_secure=False)
    try:
        bucket = s3.get_bucket(app.config['S3_BUCKET'])
    except S3ResponseError as ex:
        if ex.status != 404:
            raise
        bucket = s3.create_bucket(app.config['S3_BUCKET'])
    return bucket


@app.route('/files/upload', methods=['POST'])
def files_upload():
    if not request_wants_json():
        abort(405)

    if 'user' not in session:
        return render_json(ok=False, error='not logged in')

    uploaded_file = request.files['file']
    if uploaded_file and uploaded_file.filename:
        file = File()
        file.user_id = session['user']['id']
        file.name = uploaded_file.filename
        g.db.add(file)
        g.db.flush()
        s3_file = get_s3_bucket().new_key(str(file.id))
        s3_file.set_metadata('Content-Type', uploaded_file.content_type)
        s3_file.set_contents_from_file(uploaded_file)
        g.db.commit()

    return render_json()


import logging
from flask import g, session, redirect
from flask.ext.wtf import Form
from wtforms import TextField, PasswordField
from wtforms.validators import DataRequired
from werkzeug.security import check_password_hash
from muziq.app import app, render_template_or_json, request_wants_json, render_json
from muziq.models import User

logger = logging.getLogger(__name__)


class LoginForm(Form):
    email = TextField('email', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])


@app.route('/login', methods=('GET', 'POST'))
def login():
    if 'user' in session:
        return redirect('/')
    form = LoginForm()
    if form.validate_on_submit():
        user_data = None
        for user in g.db.query(User).filter_by(email=form.email.data):
            if check_password_hash(user.password, form.password.data):
                user_data = {'id': user.id, 'name': user.name, 'email': user.email}
                break
        if user_data is None:
            form.email.errors.append('Invalid email or password.')
        else:
            session['user'] = user_data
            if request_wants_json():
                return render_json()
            return redirect('/')
    return render_template_or_json('muziq.loginPage', ok=False,
        email=form.email.data, errors=form.errors)


@app.route('/logout')
def logout():
    if 'user' in session:
        del session['user']
    if request_wants_json():
        return render_json()
    return redirect('/')


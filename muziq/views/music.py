import logging
from flask import g, session, redirect
from muziq.app import app, render_template_or_json

logger = logging.getLogger(__name__)


@app.route('/music')
def music():
    if 'user' not in session:
        return redirect('/login')
    return render_template_or_json('muziq.musicPage')


import logging
from pysolr import Solr
from flask import g, session, request, jsonify
from muziq.app import app, render_template_or_json, render_json, request_wants_json
from mbdata.data.artist import get_artists_by_ids
from mbdata.data.recording import get_recordings_by_ids
from mbdata.data.release_group import get_release_groups_by_ids
from mbdata.api.includes import ArtistIncludes, RecordingIncludes, ReleaseGroupIncludes

logger = logging.getLogger(__name__)


def fetch_data(ids, kind, func, include):
    kind_ids = ids.get(kind)
    if not kind_ids:
        return []
    objs = func(g.db, kind_ids, include)
    return [objs[id] for id in kind_ids]


@app.route('/search/typeahead')
def search_typeahead():
    if not request_wants_json():
        abort(405)

    query = request.args.get('q')

    solr = Solr(app.config['SOLR_URL'] + 'musicbrainz/')
    search_results = solr.search(query, **{
        'defType': 'edismax',
        'fq': 'kind:artist OR kind:recording OR kind:release_group',
        'qf': 'name alias^0.9 artist^0.8',
        'mm': '100%',
        'group': 'true',
        'group.field': 'kind',
        'group.limit': 4,
    })

    ids = {}
    for group in search_results.grouped['kind']['groups']:
        kind_ids = ids[group['groupValue']] = []
        for doc in group['doclist']['docs']:
            kind_ids.append(int(doc['id'].split(':')[-1]))

    results = {
        'artist': fetch_data(ids, 'artist', get_artists_by_ids, ArtistIncludes.parse([])),
        'album': fetch_data(ids, 'release_group', get_release_groups_by_ids, ReleaseGroupIncludes.parse(['artist'])),
        'track': fetch_data(ids, 'recording', get_recordings_by_ids, RecordingIncludes.parse(['artist'])),
    }
    return jsonify(ok=True, results=results)


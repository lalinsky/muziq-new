import re
import soy.data
from flask import json


def soy_json(o):
    return soy.data.VERY_UNSAFE.ordainSanitizedJs(json.dumps(o))


def camelize(s, lower=True):
    parts = list(re.split(r'_+', s))
    if lower:
        parts[0] = parts[0].lower()
    else:
        parts[0] = parts[0].title()
    for i in range(1, len(parts)):
        parts[i] = parts[i].title()
    return ''.join(parts)


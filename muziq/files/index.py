import pika


def on_message(channel, method, properties, body):
    print 'received', method, properties, body
    channel.basic_ack(delivery_tag=method.delivery_tag)


def main():
    parameters = pika.URLParameters('amqp://muziq:muziq@localhost:5672/%2Fmuziq')
    connection = pika.BlockingConnection(parameters)

    channel = connection.channel()
    channel.queue_declare(queue='fileindex')

    print "listening", channel
    channel.basic_consume(on_message, queue='fileindex')
    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        channel.stop_consuming()

    connection.close()


if __name__ == '__main__':
    main()

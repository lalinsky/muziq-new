import os
import logging
import subprocess
import tempfile
import shutil


logger = logging.getLogger(__name__)


class NamedTemporaryDirectory(object):

    def __init__(self, dir=None):
        self.name = tempfile.mkdtemp(dir=dir)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        shutil.rmtree(self.name)


def run_sha256sum(path):
    output = subprocess.check_output(['sha256sum', path])
    return output.split()[0]


def get_tmp_dir(storage_dir):
    return os.path.join(storage_dir, 'tmp')


def get_data_dir(storage_dir, checksum):
    return os.path.join(storage_dir, 'store', checksum[:3], checksum[:6])


def store_file(storage_dir, stream, file_id):
    logger.info('uploading new file')
    with NamedTemporaryDirectory(dir=get_tmp_dir(storage_dir)) as tmp_dir:
        tmp_file_name = os.path.join(tmp_dir.name, 'uploaded')
        file.save(tmp_file_name)
        file.close()
        checksum = run_sha256sum(tmp_file_name)
        target_dir = get_data_dir(storage_dir, checksum)
        target_file_name = os.path.join(target_dir, checksum)
        logger.debug('renaming file %s to %s', tmp_file_name, target_file_name)
        if not os.path.isdir(target_dir):
            logger.info('creating dir %s', target_dir)
            os.makedirs(target_dir)
        os.rename(tmp_file_name, target_file_name)
        return checksum


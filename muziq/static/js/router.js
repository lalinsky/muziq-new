define(["module", "jquery", "backbone"], function (module, $, Backbone) {

    function parseQueryString(queryString) {
        var args = {};
        if (queryString) {
            var pairs = queryString.split("&");
            for (var i = 0; i < pairs.length; i++) {
                var pair = pairs[i].split("=", 2);
                args[pair[0]] = decodeURIComponent(pair[1]);
            }
        }
        return args;
    }

    function withParsedQueryString(callback) {
        return function () {
            arguments[0] = parseQueryString(arguments[0]);
            return callback.apply(this, arguments);
        }
    }

    return Backbone.Router.extend({

        routes: module.config().routes,

        /**
         * Initializes the router.
         * @param options.app
         */
        initialize: function (options) {
            this.app = options.app;
            this.route('*path', '404', this.on404);
            this.initConfigRoutes(module.config().routes);
            $(document).on("click", "a.app-link", $.proxy(this.onClick, this));
        },

        /**
         * Initialize routes by processing the Flask-supplied route structure.
         * @param routes
         */
        initConfigRoutes: function (routes) {
            var that = this;
            _.each(routes, function (name, path) {
                var callbackName = 'on' + name.charAt(0).toUpperCase() + name.slice(1) + 'Route';
                var callback = that[callbackName];
                that.route(path, name, withParsedQueryString(callback));
            });
        },

        /**
         * Handles a click on any link on the page.
         * @param e click event
         */
        onClick: function (e) {
            var link = $(e.currentTarget);

            if (link.hasClass('static')) {
                return;
            }

            if (link.prop('protocol') != 'http:') {
                return;
            }

            e.preventDefault();
            this.navigate(link.prop('pathname') + link.prop('search'), { trigger: true });
        },

        onLoginRoute: function (args) {
            var app = this.app;
            if (app.user.isLoggedIn()) {
                this.navigate('/', { trigger: true });
                return;
            }
            require(['views/login'], function (LoginView) {
                app.layout.content.show(LoginView, { app: app });
            })
        },

        onLogoutRoute: function (args) {
            var app = this.app;
            if (!app.user.isLoggedIn()) {
                this.navigate('/login', { trigger: true });
                return;
            }
            var router = this;
            $.ajax({
                url: "/logout",
                dataType: "json"
            }).done(function (response) {
                if (response.ok) {
                    app.user.clear();
                    router.navigate('/login', { trigger: true });
                }
            });
        },

        onIndexRoute: function (args) {
            var app = this.app;
            if (!app.user.isLoggedIn()) {
                this.navigate('/login', { trigger: true });
                return;
            }
            require(['layouts/app', 'views/index'], function (AppLayout, IndexView) {
                app.layout.ensureAppLayout().main.show(IndexView, { app: app });
            })
        },

        onMusicRoute: function (args) {
            var app = this.app;
            if (!app.user.isLoggedIn()) {
                this.navigate('/login', { trigger: true });
                return;
            }
            require(['layouts/app', 'views/music'], function (AppLayout, MusicView) {
                app.layout.ensureAppLayout().main.show(MusicView, { app: app });
            })
        },

        onFilesRoute: function (args) {
            var app = this.app;
            if (!app.user.isLoggedIn()) {
                this.navigate('/login', { trigger: true });
                return;
            }
            require(['layouts/app', 'views/files'], function (AppLayout, FilesView) {
                var appLayout = app.layout.ensureAppLayout();
                if (!_.isUndefined(appLayout.main.currentView) && appLayout.main.currentView instanceof FilesView) {
                    appLayout.main.currentView.loadData(args.page);
                }
                else {
                    appLayout.main.show(FilesView, { app: app });
                }
            })
        },

        on404: function (args) {
            console.log('not found', args);
        }

    });

});

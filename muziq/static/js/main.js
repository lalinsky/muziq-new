require.config({
    paths: {
        "jquery": "../vendor/js/jquery",
        "jquery.jplayer": "../vendor/js/jquery.jplayer",
        "jquery.typeahead": "../vendor/js/jquery.typeahead",
        "bootstrap": "../vendor/js/bootstrap",
        "underscore": "../vendor/js/underscore",
        "backbone": "../vendor/js/backbone",
        "backbone.marionette": "../vendor/js/backbone.marionette",
        "backbone.wreqr": "../vendor/js/backbone.wreqr",
        "backbone.babysitter": "../vendor/js/backbone.babysitter",
        "dropzone": "../vendor/js/dropzone",
        "soy": "../vendor/js/soyutils"
    },
    shim: {
        "soy": { exports: "soy" },
        "bootstrap": { deps: ["jquery"] },
        "jquery.typeahead": { deps: ["jquery"] }
    }
});

require(["jquery", "bootstrap", "app"], function ($, _bootstrap, app) {
    $(function () { app.start() });
});


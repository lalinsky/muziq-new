// This file was automatically generated from base.soy.
// Please don't edit this file by hand.

if (typeof muziq == 'undefined') { var muziq = {}; }


muziq.mainPage = function(opt_data, opt_ignored, opt_ijData) {
  return '<!DOCTYPE html><html><head><meta charset="utf-8"><title>' + ((opt_data.title) ? soy.$$escapeHtmlRcdata(opt_data.title) + ' | ' : '') + 'MZQ</title><link rel="stylesheet" href="/static/css/main.css"><script>var require = {config: {router: {routes: ' + soy.$$escapeJsValue(opt_ijData.routesJson) + '}}};<\/script><script data-main="/static/js/main.js" src="/static/vendor/js/require.js"><\/script></head><body><div class="outlet">' + muziq.mainLayout(opt_data, null, opt_ijData) + '</div><div class="jp-jplayer" id="jp_jplayer"></div></body></html>';
};


muziq.appPage = function(opt_data, opt_ignored, opt_ijData) {
  return muziq.mainPage({title: opt_data.title, user: opt_data.user, content: '<div class="outlet">' + muziq.appLayout({content: '<div class="outlet">' + soy.$$filterNoAutoescape(opt_data.content) + '</div>'}, null, opt_ijData) + '</div>'}, null, opt_ijData);
};


muziq.mainLayout = function(opt_data, opt_ignored, opt_ijData) {
  return '<div id="menu"><div class="outlet">' + muziq.menu(opt_data, null, opt_ijData) + '</div></div><div id="content">' + soy.$$filterNoAutoescape(opt_data.content) + '</div>';
};


muziq.menu = function(opt_data, opt_ignored, opt_ijData) {
  opt_data = opt_data || {};
  return '<div class="navbar navbar-inverse navbar-fixed-top" role="navigation"><div class="container-fluid"><div class="navbar-header"><button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a class="navbar-brand app-link" href="/"><img src="/static/img/logo-header.png" title="MZQ"></a></div><div class="navbar-collapse collapse">' + ((opt_data.user) ? '<ul class="nav navbar-nav"><li><a href="/music" class="app-link">Music</a></li><li><a href="/files" class="app-link">Files</a></li></ul><div id="jp_container_1" class="navbar-player"><div class="jp-gui"><div class="jp-controls"><a href="#" class="jp-back disabled" title="Back"><i class="icon-backward2"></i></a><a href="#" class="jp-play" title="Play"><i class="icon-play"></i></a><a href="#" class="jp-pause" style="display:none" title="Pause"><i class="icon-pause"></i></a><a href="#" class="jp-next disabled" title="Next"><i class="icon-forward2"></i></a></div><div class="jp-status"><div class="jp-title">Song<span class="artist"> - Artist</span></div><div class="jp-progress"><div class="jp-seek-bar" style="width:0%"><div class="jp-play-bar" style="width:%0"></div></div></div><div class="jp-time-holder"><div class="jp-current-time">00:00</div></div></div></div></div>' : '') + '<form action="/search" class="navbar-form navbar-right" role="search"><div class="form-group"><input type="text" name="q" placeholder="Search" class="form-control search-input"></div></form><ul class="nav navbar-nav navbar-right">' + ((opt_data.user) ? '<li class="dropdown"><a href="/me" id="user" class="dropdown-toggle" data-toggle="dropdown" data-id="' + soy.$$escapeHtmlAttribute(opt_data.user.id) + '" data-email="' + soy.$$escapeHtmlAttribute(opt_data.user.email) + '" data-name="' + soy.$$escapeHtmlAttribute(opt_data.user.name) + '" class="user">' + soy.$$escapeHtml(opt_data.user.name) + ' <b class="caret"></b></a><ul class="dropdown-menu" role="menu"><li role="presentation"><a href="/me" class="app-link">Profile</a></li><li role="presentation"><a href="/settings" class="app-link">Settings</a></li><li role="presentation" class="divider"></li><li role="presentation"><a href="/logout" class="app-link">Log out</a></li></ul></li>' : '<li><a href="/login" class="app-link">Login</a></li>') + '</ul></div></div></div>';
};


muziq.appLayout = function(opt_data, opt_ignored, opt_ijData) {
  return '<div class="container-fluid"><div class="row"><div id="sidebar" class="col-sm-2"><div class="outlet">' + muziq.sidebar(null, null, opt_ijData) + '</div></div><div class="col-sm-10"><div id="main">' + soy.$$filterNoAutoescape(opt_data.content) + '</div></div></div></div>';
};


muziq.sidebar = function(opt_data, opt_ignored, opt_ijData) {
  return '<ul class="nav nav-stacked"><li><a href="/music/library">Library</a></li><li><a href="/music/radio">Radio</a></li></ul>';
};

;
// This file was automatically generated from files.soy.
// Please don't edit this file by hand.

if (typeof muziq == 'undefined') { var muziq = {}; }


muziq.filesPage = function(opt_data, opt_ignored, opt_ijData) {
  return muziq.appPage({title: 'Files', user: opt_data.user, content: muziq.filesPageContent(opt_data, null, opt_ijData)}, null, opt_ijData);
};


muziq.filesPageContent = function(opt_data, opt_ignored, opt_ijData) {
  var output = '<div class="pull-right"><a href="javascript:void(0)" class="btn btn-sm btn-primary upload">Upload</a></div><h1>My files</h1>';
  if (opt_data.files) {
    output += '<table class="table table-striped table-condensed"><thead><tr><th></th><th>Filename</th><th>Uploaded</th></tr></thead><tbody>';
    var fileList76 = opt_data.files;
    var fileListLen76 = fileList76.length;
    for (var fileIndex76 = 0; fileIndex76 < fileListLen76; fileIndex76++) {
      var fileData76 = fileList76[fileIndex76];
      output += '<tr>' + muziq.fileRow({file: fileData76}, null, opt_ijData) + '</tr>';
    }
    output += '</tbody></table>' + muziq.ui.pagination(opt_data, null, opt_ijData);
  } else {
    output += '<p>You have no files in your library yet!</p>';
  }
  return output;
};


muziq.uploadDialog = function(opt_data, opt_ignored, opt_ijData) {
  return '<div class="modal fade" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title">Upload files</h4></div><div class="modal-body upload"><div class="upload-drop"><div class="upload-info"><div class="help">Select music files to upload. You can select more than one file at a time. You can also drag and drop files anywhere on this page to start uploading.</div></div></div></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button><button type="button" class="btn btn-primary">Select files</button></div></div></div></div>';
};


muziq.uploadPreviewsTable = function(opt_data, opt_ignored, opt_ijData) {
  return '<table class="upload-previews table table-condensed"><thead><tr><th>File</th><th>Status</th></tr></thead><tbody></tbody></table>';
};


muziq.uploadPreviewRow = function(opt_data, opt_ignored, opt_ijData) {
  return '<tr class="dz-preview dz-file-preview"><td><span data-dz-name></span> (<span data-dz-size></span>)</td><td><span class="dz-progress" data-dz-uploadprogress></span><span class="dz-success-mark">✔</span><span class="dz-error-mark">✘</span></td></tr>';
};


muziq.fileRow = function(opt_data, opt_ignored, opt_ijData) {
  return '<td><a href="javascript:void(0)" class="play-btn" data-track="' + soy.$$escapeHtmlAttribute(opt_data.file.id) + '"><i class="icon-play2"></i></a></td><td>' + soy.$$escapeHtml(opt_data.file.name) + '</td><td>' + soy.$$escapeHtml(opt_data.file.uploaded) + '</td>';
};

;
// This file was automatically generated from index.soy.
// Please don't edit this file by hand.

if (typeof muziq == 'undefined') { var muziq = {}; }


muziq.indexPage = function(opt_data, opt_ignored, opt_ijData) {
  opt_data = opt_data || {};
  return muziq.appPage({user: opt_data.user, content: muziq.indexPageContent(opt_data, null, opt_ijData)}, null, opt_ijData);
};


muziq.indexPageContent = function(opt_data, opt_ignored, opt_ijData) {
  return '<h1>Hello World!</h1>';
};

;
// This file was automatically generated from login.soy.
// Please don't edit this file by hand.

if (typeof muziq == 'undefined') { var muziq = {}; }


muziq.loginPage = function(opt_data, opt_ignored, opt_ijData) {
  return muziq.mainPage({title: 'Sign in', content: muziq.loginPageContent(opt_data, null, opt_ijData)}, null, opt_ijData);
};


muziq.loginPageContent = function(opt_data, opt_ignored, opt_ijData) {
  opt_data = opt_data || {};
  var output = '<div class="container"><form class="form-login" role="form" action="/login" method="post"><h2 class="form-login-heading">Please log in</h2><div class="form-group' + ((opt_data.errors.email) ? ' has-error has-feedback' : '') + '"><input id="email" name="email" type="email" class="form-control" placeholder="Email address" required autofocus value="' + ((opt_data.email) ? soy.$$escapeHtmlAttribute(opt_data.email) : '') + '">';
  if (opt_data.errors.email) {
    output += '<span class="help-block">';
    var errorList129 = opt_data.errors.email;
    var errorListLen129 = errorList129.length;
    for (var errorIndex129 = 0; errorIndex129 < errorListLen129; errorIndex129++) {
      var errorData129 = errorList129[errorIndex129];
      output += soy.$$escapeHtml(errorData129);
    }
    output += '</span>';
  }
  output += '</div><div class="form-group' + ((opt_data.errors.password) ? ' has-error has-feedback' : '') + '"><input id="password" name="password" type="password" class="form-control" placeholder="Password" required>';
  if (opt_data.errors.password) {
    output += '<span class="help-block">';
    var errorList141 = opt_data.errors.password;
    var errorListLen141 = errorList141.length;
    for (var errorIndex141 = 0; errorIndex141 < errorListLen141; errorIndex141++) {
      var errorData141 = errorList141[errorIndex141];
      output += soy.$$escapeHtml(errorData141);
    }
    output += '</span>';
  }
  output += '</div><label class="checkbox"><input name="remember_me" type="checkbox" value="yes"> Remember me</label><button class="btn btn-primary btn-block" type="submit">Log in</button><div class="form-login-help pull-right"><a href="/help/login" class="app-link">Need help?</a></div></form></div>';
  return output;
};

;
// This file was automatically generated from music.soy.
// Please don't edit this file by hand.

if (typeof muziq == 'undefined') { var muziq = {}; }


muziq.musicPage = function(opt_data, opt_ignored, opt_ijData) {
  opt_data = opt_data || {};
  return muziq.appPage({title: 'Music', user: opt_data.user, content: muziq.musicPageContent(opt_data, null, opt_ijData)}, null, opt_ijData);
};


muziq.musicPageContent = function(opt_data, opt_ignored, opt_ijData) {
  return '<h1>My Library</h1>';
};

;
// This file was automatically generated from search.soy.
// Please don't edit this file by hand.

if (typeof muziq == 'undefined') { var muziq = {}; }
if (typeof muziq.search == 'undefined') { muziq.search = {}; }


muziq.search.artistSuggestionHeader = function(opt_data, opt_ignored, opt_ijData) {
  return (! opt_data.isEmpty) ? '<strong class="tt-dropdown-header">Artists</strong>' : '';
};


muziq.search.albumSuggestionHeader = function(opt_data, opt_ignored, opt_ijData) {
  return (! opt_data.isEmpty) ? '<strong class="tt-dropdown-header">Albums</strong>' : '';
};


muziq.search.trackSuggestionHeader = function(opt_data, opt_ignored, opt_ijData) {
  return (! opt_data.isEmpty) ? '<strong class="tt-dropdown-header">Tracks</strong>' : '';
};


muziq.search.seeAllResultsSuggestion = function(opt_data, opt_ignored, opt_ijData) {
  return '<a href="/search?q=' + soy.$$escapeUri(opt_data.query) + '" class="app-link">See all results</a>';
};


muziq.search.artistSuggestion = function(opt_data, opt_ignored, opt_ijData) {
  return '<a href="/music/artist/' + soy.$$escapeHtmlAttribute(opt_data.id) + '" class="app-link">' + soy.$$escapeHtml(opt_data.name) + ((opt_data.comment) ? ' <small class="text-muted">(' + soy.$$escapeHtml(opt_data.comment) + ')</small>' : '') + '</a>';
};


muziq.search.albumSuggestion = function(opt_data, opt_ignored, opt_ijData) {
  return '<a href="/music/album/' + soy.$$escapeHtmlAttribute(opt_data.id) + '" class="app-link">' + soy.$$escapeHtml(opt_data.name) + '<span class="text-muted"> - ' + soy.$$escapeHtml(opt_data.artist) + '</span></a>';
};


muziq.search.trackSuggestion = function(opt_data, opt_ignored, opt_ijData) {
  return '<a href="/music/track/' + soy.$$escapeHtmlAttribute(opt_data.id) + '" class="app-link">' + soy.$$escapeHtml(opt_data.name) + '<span class="text-muted"> - ' + soy.$$escapeHtml(opt_data.artist) + '</span></a>';
};

;
// This file was automatically generated from ui.soy.
// Please don't edit this file by hand.

if (typeof muziq == 'undefined') { var muziq = {}; }
if (typeof muziq.ui == 'undefined') { muziq.ui = {}; }


muziq.ui.pagination = function(opt_data, opt_ignored, opt_ijData) {
  var output = '';
  if (opt_data.pagination.pageList && (opt_data.pagination.hasPrev || opt_data.pagination.hasNext)) {
    output += '<ul class="pagination">' + ((opt_data.pagination.hasPrev) ? '<li><a href="/files?page=' + soy.$$escapeUri(opt_data.pagination.page - 1) + '" class="app-link">«</a></li>' : '<li class="disabled"><span>«</span></li>');
    var pageList212 = opt_data.pagination.pageList;
    var pageListLen212 = pageList212.length;
    for (var pageIndex212 = 0; pageIndex212 < pageListLen212; pageIndex212++) {
      var pageData212 = pageList212[pageIndex212];
      output += (pageData212) ? (pageData212 != opt_data.pagination.page) ? '<li><a href="/files' + ((pageData212 != 1) ? '?page=' + soy.$$escapeUri(pageData212) : '') + '" class="app-link">' + soy.$$escapeHtml(pageData212) + '</a></li>' : '<li class="active"><span>' + soy.$$escapeHtml(pageData212) + '</span></li>' : '<li><span>…</span></li>';
    }
    output += ((opt_data.pagination.hasNext) ? '<li><a href="/files?page=' + soy.$$escapeUri(opt_data.pagination.page + 1) + '" class="app-link">»</a></li>' : '<li class="disabled"><span>»</span></li>') + '</ul>';
  }
  return output;
};


muziq.ui.searchArtistHeader = function(opt_data, opt_ignored, opt_ijData) {
  return (! opt_data.isEmpty) ? '<strong class="tt-dropdown-header">Artists</strong>' : '';
};


muziq.ui.searchRecordingHeader = function(opt_data, opt_ignored, opt_ijData) {
  return (! opt_data.isEmpty) ? '<strong class="tt-dropdown-header">Tracks</strong>' : '';
};


muziq.ui.searchReleaseGroupHeader = function(opt_data, opt_ignored, opt_ijData) {
  return (! opt_data.isEmpty) ? '<strong class="tt-dropdown-header">Release group</strong>' : '';
};


muziq.ui.searchLabelHeader = function(opt_data, opt_ignored, opt_ijData) {
  return (! opt_data.isEmpty) ? '<strong class="tt-dropdown-header">Labels</strong>' : '';
};


muziq.ui.searchArtistSuggestion = function(opt_data, opt_ignored, opt_ijData) {
  return '<a href="/music/artist/' + soy.$$escapeHtmlAttribute(opt_data.id) + '" class="app-link">' + soy.$$escapeHtml(opt_data.name) + ((opt_data.comment) ? ' <small class="text-muted">(' + soy.$$escapeHtml(opt_data.comment) + ')</small>' : '') + '</a>';
};


muziq.ui.searchLabelSuggestion = function(opt_data, opt_ignored, opt_ijData) {
  return '<a href="/music/label/' + soy.$$escapeHtmlAttribute(opt_data.id) + '" class="app-link">' + soy.$$escapeHtml(opt_data.name) + ((opt_data.comment) ? ' <small class="text-muted">(' + soy.$$escapeHtml(opt_data.comment) + ')</small>' : '') + '</a>';
};


muziq.ui.searchRecordingSuggestion = function(opt_data, opt_ignored, opt_ijData) {
  return '<a href="/music/recording/' + soy.$$escapeHtmlAttribute(opt_data.id) + '" class="app-link">' + soy.$$escapeHtml(opt_data.name) + '<span class="text-muted"> - ' + soy.$$escapeHtml(opt_data.artist) + '</span></a>';
};


muziq.ui.searchShowAllResults = function(opt_data, opt_ignored, opt_ijData) {
  return '<a href="/search?q=' + soy.$$escapeUri(opt_data.query) + '" class="app-link">Show all results</a>';
};

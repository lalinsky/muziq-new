define(["backbone.marionette", "underscore"], function (Marionette, _) {

    var Puppet = {};

    Puppet.Region = Marionette.Region.extend({

        show: function (ViewType, options) {
            this.ensureEl();

            var view, show = true;

            if (!_.isFunction(ViewType)) {
                // regular view object, nothing special to be done
                view = ViewType;
            }
            else {
                // view type, we need to construct it
                var $viewEl = this.$el.children().first();
                if (_.isUndefined(this.currentView) && $viewEl.length > 0) {
                    // initial page load with pre-rendered content
                    view = new ViewType(_.extend(options, { el: $viewEl }));
                    if (_.isFunction(view.onRender)) {
                        view.onRender();
                    }
                    this.attachView(view);
                    show = false;
                }
                else {
                    // page load with client-side rendering
                    view = new ViewType(options);
                }
            }

            if (show) {
                Marionette.Region.prototype.show.call(this, view);
            }

            return view;
        }

    });

    Puppet.Layout = Marionette.Layout.extend({

        regionType: Puppet.Region,

        initialize: function (options) {
            this.app = options.app;
        }

    });

    Puppet.View = Marionette.ItemView.extend({

        tagName: 'div',
        className: 'outlet'

    });

    return Puppet;

});


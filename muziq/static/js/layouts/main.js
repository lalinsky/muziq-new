define([
    "puppet",
    "templates",
    "layouts/app",
    "jquery",
    "jquery.typeahead"
], function (Puppet, templates, AppLayout, $, _x) {

    return Puppet.Layout.extend({

        template: templates.muziq.mainLayout,

        regions: {
            menu: "#menu",
            content: "#content"
        },

        ensureAppLayout: function () {
            if (!(this.content.currentView instanceof AppLayout)) {
                this.content.show(AppLayout, { app: this.app });
            }
            return this.content.currentView;
        }

    });

});

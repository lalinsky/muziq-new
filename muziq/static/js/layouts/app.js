define(["puppet", "templates"], function (Puppet, templates) {

    return Puppet.Layout.extend({

        template: templates.muziq.appLayout,

        regions: {
            sidebar: "#sidebar",
            main: "#main"
        }

    });

});

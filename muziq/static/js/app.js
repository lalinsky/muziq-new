define([
    "backbone",
    "backbone.marionette",
    "jquery",
    "jquery.jplayer",
    "router",
    "layouts/main",
    "views/menu"
], function (Backbone, Marionette, $, _ignore, Router, MainLayout, MenuView) {

    var User = Backbone.Model.extend({

        isLoggedIn: function () {
            return this.has("name");
        }

    });

    var app = new Marionette.Application();

    app.addInitializer(function (options) {
        this.initialized = false;
        this.user = new User();

        this.layout = new MainLayout({ app: this, el: Backbone.$("body") });

        this.menuView = this.layout.menu.show(MenuView, { app: this });

        this.user.set(this.menuView.initialUserData());

        this.router = new Router({ app: this });
    });

    app.on("initialize:after", function (options) {
        Backbone.history.start({ pushState: true });
        app.initialized = true;

        console.log("jplaqyer");
        app.player = $("#jp_jplayer").jPlayer({
            swfPath: "/static/vendor",
            preload: "none",
            supplied: "mp3",
            error: function (e) {
                console.log("jPlayer error", e);
            }
        });

    });

    return app;

});

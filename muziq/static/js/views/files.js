define(["puppet", "templates", "dropzone", "jquery", "underscore"], function (Puppet, templates, Dropzone, $, _) {

    Dropzone.createElement = function (html) {
        return $(html)[0];
    }

    var File = Backbone.Model.extend({
        defaults: {
            name: null,
            uploaded: null
        }
    });

    var Pagination = Backbone.Model.extend({
        defaults: {
            page: null,
            perPage: null,
            totalCount: null,
            pages: null,
            hasNext: null,
            hasPrev: null,
            pageList: []
        }
    });

    var FileList = Backbone.Collection.extend({
        model: File
    });

    return Puppet.View.extend({

        template: templates.muziq.filesPageContent,

        events: {
            'click a.play-btn': "onPlay",
            "click a.upload": "showUploadDialog"
        },

        initialize: function (options) {
            if (options.el) {
                this.files = new FileList(this.getInitialFilesData());
            }
            else {
                this.files = new FileList();
                this.loadData();
            }
            this.app = options.app;
            this.files.on("change", this.render);
            this.pagination = new Pagination();
        },

        /**
         * Construct initial files data from the pre-rendered template.
         */
        getInitialFilesData: function () {
            var files = [];
            this.$("tbody tr").each(function () {
                var $tr = $(this);
                var $tds = $tr.find("td");
                files.push({
                    name: $($tds[0]).text(),
                    uploaded: $($tds[1]).text()
                });
            });
            return files;
        },

        /**
         * Load a single page of files from the server.
         */
        loadData: function (page) {
            var that = this, data = {};
            if (page && page > 1) { data.page = page; }
            $.ajax({ url: "/files", data: data, dataType: "json" })
                .done(function (response) {
                    if (response.ok) {
                        that.files.reset(response.files);
                        that.pagination.set(response.pagination);
                        that.render();
                    }
                })
                .fail(function () {
                    alert("request failed");
                });
        },

        serializeData: function () {
            var data = {};
            data.files = this.files.toJSON();
            data.pagination = this.pagination.toJSON();
            return data;
        },

        onPlay: function (e) {
            var that = this;
            var trackId = $(e.currentTarget).data('track');
            $.ajax({ url: "/files/stream", data: { "id": trackId }, dataType: "json" })
                .done(function (response) {
                    $(that.app.player)
                        .jPlayer("setMedia", { "mp3": response.streamUrl })
                        .jPlayer("play");
                });
        },

        showUploadDialog: function () {
            var $dialog = $(templates.muziq.uploadDialog()).appendTo("body");
            var dropzone = new Dropzone($dialog[0], {
                url: "/files/upload",
                createImageThumbnails: false,
                previewTemplate: templates.muziq.uploadPreviewRow(),
                clickable: $dialog.find(".btn-primary")[0],
                acceptedFiles: "audio/*,.ogg,.flac,.m4a,.mp3",
                addedfile: function () {
                    if ($dialog.find(".upload-info tbody").length === 0) {
                        $dialog.find(".upload-info").html(templates.muziq.uploadPreviewsTable());
                        this.previewsContainer = $dialog.find(".upload-info tbody")[0];
                    }
                    Dropzone.prototype.defaultOptions.addedfile.apply(this, arguments);
                }
            });
            $dialog.on("hidden.bs.modal", function (e) {
                dropzone.destroy();
                $dialog.remove();
            });
            $dialog.modal({
                backdrop: "static"
            });
        }

    });

});

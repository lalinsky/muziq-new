define(["puppet", "templates"], function (Puppet, templates) {

    return Puppet.View.extend({

        template: templates.muziq.musicPageContent

    });

});

define(["puppet", "templates"], function (Puppet, templates) {

    return Puppet.View.extend({

        template: templates.muziq.indexPageContent,

        serializeData: function () {
            return { user: {} };
        }

    });

});

define(["puppet", "templates"], function (Puppet, templates) {

    return Puppet.View.extend({
        
        template: templates.muziq.loginPageContent,

        events: {
            "submit form": "onFormSubmit"
        },

        initialize: function (options) {
            this.app = options.app;
            this.email = "";
            this.errors = [];
        },

        serializeData: function () {
            return { email: this.email, errors: this.errors };
        },

        onFormSubmit: function (e) {
            e.preventDefault();

            var that = this;
            var form = $(e.currentTarget);

            $.ajax({
                url: "/login",
                method: "post",
                data: {
                    email: form.find('#email').val(),
                    password: form.find('#password').val(),
                },
                dataType: "json"
            }).done(function (response) {
                if (response.ok) {
                    that.app.user.set(response.user);
                    that.app.router.navigate('/', { trigger: true });
                } else {
                    that.email = response.email;
                    that.errors = response.errors;
                    that.render();
                }
            });
        }

    });

});

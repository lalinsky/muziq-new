define(["puppet", "templates", "jquery", "underscore"], function (Puppet, templates, $, _) {

    var Search = function () {
        this.requestID = 0;
        this.requests = {};
        _.bindAll(this, 'getArtistSource', 'getAlbumSource', 'getTrackSource');
    };

    _.extend(Search.prototype, {

        runSearch: function (query, callbacks) {
            $.ajax({
                url: "/search/typeahead",
                data: { q: query },
                dataType: "json"
            }).done(function (data) {
                _.each(callbacks, function (callback, name) {
                    callback(data.results);
                });
            });
        },

        runSearchRequest: function (requestID) {
            this.runSearch(
                this.requests[requestID].query,
                this.requests[requestID].callbacks);
            delete this.requests[requestID];
        },

        maybeRunSearch: function (query, key, callback) {
            if (_.has(this.requests, this.requestID)) {
                clearTimeout(this.requests[this.requestID].timeoutID);
            } else {
                this.requestID += 1;
                this.requests[this.requestID] = { callbacks: {} };
            }
            this.requests[this.requestID].query = query;
            this.requests[this.requestID].callbacks[key] = callback;
            this.requests[this.requestID].timeoutID = setTimeout(
                _.bind(this.runSearchRequest, this, this.requestID), 200);
        },

        getArtistSource: function (query, callback) {
            this.maybeRunSearch(query, 'artist', function (results) {
                callback(results.artist);
            });
        },

        getAlbumSource: function (query, callback) {
            this.maybeRunSearch(query, 'album', function (results) {
                callback(results.album);
            });
        },

        getTrackSource: function (query, callback) {
            this.maybeRunSearch(query, 'track', function (results) {
                callback(results.track);
            });
        }

    });

    return Puppet.View.extend({

        template: templates.muziq.menu,

        ui: {
            searchInput: ".search-input"
        },

        initialize: function (options) {
            this.app = options.app;
            this.app.user.on("change", this.render, this);
            this.search = new Search();
        },

        onRender: function () {
            var app = this.app;
            var $input = $(this.ui.searchInput);
            $input.typeahead(
                {
                    highlight: true,
                    autoselect: true,
                    hint: false
                },
                {
                    source: function (query, cb) { cb([{"query": query}]) },
                    name: "search",
                    displayKey: "query",
                    templates: {
                        suggestion: templates.muziq.search.seeAllResultsSuggestion
                    }
                },
                {
                    source: this.search.getArtistSource,
                    name: "artist",
                    displayKey: "name",
                    templates: {
                        header: templates.muziq.search.artistSuggestionHeader,
                        suggestion: templates.muziq.search.artistSuggestion
                    }
                },
                {
                    source: this.search.getAlbumSource,
                    name: "album",
                    displayKey: "name",
                    templates: {
                        header: templates.muziq.search.albumSuggestionHeader,
                        suggestion: templates.muziq.search.albumSuggestion
                    }
                },
                {
                    source: this.search.getTrackSource,
                    name: "track",
                    displayKey: "name",
                    templates: {
                        header: templates.muziq.search.trackSuggestionHeader,
                        suggestion: templates.muziq.search.trackSuggestion
                    }
                }
            );
            $input.on("typeahead:selected", function (e, suggestion, dataset) {
                if (dataset === "search") {
                    app.router.navigate("/search?q=" + encodeURIComponent(suggestion.query), { trigger: true });
                } else if (dataset === "artist") {
                    app.router.navigate("/music/artist/" + suggestion.id, { trigger: true });
                } else if (dataset === "album") {
                    app.router.navigate("/music/album/" + suggestion.id, { trigger: true });
                } else if (dataset === "track") {
                    app.router.navigate("/music/track/" + suggestion.id, { trigger: true });
                }
            });
        },

        initialUserData: function () {
            var $user = this.$("#user");
            if ($user.length > 0) {
                return {
                    id: $user.data("id"),
                    email: $user.data("email"),
                    name: $user.data("name")
                };
            }
            return {};
        },

        serializeData: function () {
            var data = {};
            if (this.app.user.isLoggedIn()) {
                data.user = this.app.user.attributes;
            }
            return data;
        }

    });

});

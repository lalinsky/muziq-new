# This file was automatically generated from base.soy.
# Please don't edit this file by hand.

import soy.utils
import soy.data
import soy.bidi
import math
import random

try:
    registry
except NameError:
    raise ImportError('This file should be imported using soy.tofu.SoyTofu')



def muziq__mainPage(data=None, ijData=None):
    output = u'<!DOCTYPE html><html><head><meta charset=\"utf-8\"><title>'
    if soy.utils.getDataSingleAttr(data, 'title'):
        output += soy.utils.escapeHtmlRcdata(soy.utils.getDataSingleAttr(data, 'title')) + u' | '
    output += u'MZQ</title><link rel=\"stylesheet\" href=\"/static/css/main.css\"><script>var require = {config: {router: {routes: ' + soy.utils.escapeJsValue(soy.utils.getDataSingleAttr(ijData, 'routesJson')) + u'}}};</script><script data-main=\"/static/js/main.js\" src=\"/static/vendor/js/require.js\"></script></head><body><div class=\"outlet\">' + registry.lookup('muziq.mainLayout')(data, ijData=ijData) + u'</div><div class=\"jp-jplayer\" id=\"jp_jplayer\"></div></body></html>'
    return output

registry.register('muziq.mainPage', muziq__mainPage)


def muziq__appPage(data=None, ijData=None):
    return registry.lookup('muziq.mainPage')({'title': soy.utils.getDataSingleAttr(data, 'title'), 'user': soy.utils.getDataSingleAttr(data, 'user'), 'content': u'<div class=\"outlet\">' + registry.lookup('muziq.appLayout')({'content': u'<div class=\"outlet\">' + soy.utils.filterNoAutoescape(soy.utils.getDataSingleAttr(data, 'content')) + u'</div>'}, ijData=ijData) + u'</div>'}, ijData=ijData)

registry.register('muziq.appPage', muziq__appPage)


def muziq__mainLayout(data=None, ijData=None):
    return u'<div id=\"menu\"><div class=\"outlet\">' + registry.lookup('muziq.menu')(data, ijData=ijData) + u'</div></div><div id=\"content\">' + soy.utils.filterNoAutoescape(soy.utils.getDataSingleAttr(data, 'content')) + u'</div>'

registry.register('muziq.mainLayout', muziq__mainLayout)


def muziq__menu(data=None, ijData=None):
    data = {} if data is None else data
    output = u'<div class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\"><div class=\"container-fluid\"><div class=\"navbar-header\"><button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\"><span class=\"sr-only\">Toggle navigation</span><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span></button><a class=\"navbar-brand app-link\" href=\"/\"><img src=\"/static/img/logo-header.png\" title=\"MZQ\"></a></div><div class=\"navbar-collapse collapse\">'
    if soy.utils.getDataSingleAttr(data, 'user'):
        output += u'<ul class=\"nav navbar-nav\"><li><a href=\"/music\" class=\"app-link\">Music</a></li><li><a href=\"/files\" class=\"app-link\">Files</a></li></ul><div id=\"jp_container_1\" class=\"navbar-player\"><div class=\"jp-gui\"><div class=\"jp-controls\"><a href=\"#\" class=\"jp-back disabled\" title=\"Back\"><i class=\"icon-backward2\"></i></a><a href=\"#\" class=\"jp-play\" title=\"Play\"><i class=\"icon-play\"></i></a><a href=\"#\" class=\"jp-pause\" style=\"display:none\" title=\"Pause\"><i class=\"icon-pause\"></i></a><a href=\"#\" class=\"jp-next disabled\" title=\"Next\"><i class=\"icon-forward2\"></i></a></div><div class=\"jp-status\"><div class=\"jp-title\">Song<span class=\"artist\"> - Artist</span></div><div class=\"jp-progress\"><div class=\"jp-seek-bar\" style=\"width:0%\"><div class=\"jp-play-bar\" style=\"width:%0\"></div></div></div><div class=\"jp-time-holder\"><div class=\"jp-current-time\">00:00</div></div></div></div></div>'
    output += u'<form action=\"/search\" class=\"navbar-form navbar-right\" role=\"search\"><div class=\"form-group\"><input type=\"text\" name=\"q\" placeholder=\"Search\" class=\"form-control search-input\"></div></form><ul class=\"nav navbar-nav navbar-right\">'
    if soy.utils.getDataSingleAttr(data, 'user'):
        output += u'<li class=\"dropdown\"><a href=\"/me\" id=\"user\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" data-id=\"' + soy.utils.escapeHtmlAttribute(soy.utils.getDataSingleAttr(soy.utils.getDataSingleAttr(data, 'user'), 'id')) + u'\" data-email=\"' + soy.utils.escapeHtmlAttribute(soy.utils.getDataSingleAttr(soy.utils.getDataSingleAttr(data, 'user'), 'email')) + u'\" data-name=\"' + soy.utils.escapeHtmlAttribute(soy.utils.getDataSingleAttr(soy.utils.getDataSingleAttr(data, 'user'), 'name')) + u'\" class=\"user\">' + soy.utils.escapeHtml(soy.utils.getDataSingleAttr(soy.utils.getDataSingleAttr(data, 'user'), 'name')) + u' <b class=\"caret\"></b></a><ul class=\"dropdown-menu\" role=\"menu\"><li role=\"presentation\"><a href=\"/me\" class=\"app-link\">Profile</a></li><li role=\"presentation\"><a href=\"/settings\" class=\"app-link\">Settings</a></li><li role=\"presentation\" class=\"divider\"></li><li role=\"presentation\"><a href=\"/logout\" class=\"app-link\">Log out</a></li></ul></li>'
    else:
        output += u'<li><a href=\"/login\" class=\"app-link\">Login</a></li>'
    output += u'</ul></div></div></div>'
    return output

registry.register('muziq.menu', muziq__menu)


def muziq__appLayout(data=None, ijData=None):
    return u'<div class=\"container-fluid\"><div class=\"row\"><div id=\"sidebar\" class=\"col-sm-2\"><div class=\"outlet\">' + registry.lookup('muziq.sidebar')(None, ijData=ijData) + u'</div></div><div class=\"col-sm-10\"><div id=\"main\">' + soy.utils.filterNoAutoescape(soy.utils.getDataSingleAttr(data, 'content')) + u'</div></div></div></div>'

registry.register('muziq.appLayout', muziq__appLayout)


def muziq__sidebar(data=None, ijData=None):
    return u'<ul class=\"nav nav-stacked\"><li><a href=\"/music/library\">Library</a></li><li><a href=\"/music/radio\">Radio</a></li></ul>'

registry.register('muziq.sidebar', muziq__sidebar)

# This file was automatically generated from files.soy.
# Please don't edit this file by hand.

import soy.utils
import soy.data
import soy.bidi
import math
import random

try:
    registry
except NameError:
    raise ImportError('This file should be imported using soy.tofu.SoyTofu')



def muziq__filesPage(data=None, ijData=None):
    return registry.lookup('muziq.appPage')({'title': u'Files', 'user': soy.utils.getDataSingleAttr(data, 'user'), 'content': registry.lookup('muziq.filesPageContent')(data, ijData=ijData)}, ijData=ijData)

registry.register('muziq.filesPage', muziq__filesPage)


def muziq__filesPageContent(data=None, ijData=None):
    output = u'<div class=\"pull-right\"><a href=\"javascript:void(0)\" class=\"btn btn-sm btn-primary upload\">Upload</a></div><h1>My files</h1>'
    if soy.utils.getDataSingleAttr(data, 'files'):
        output += u'<table class=\"table table-striped table-condensed\"><thead><tr><th></th><th>Filename</th><th>Uploaded</th></tr></thead><tbody>'
        fileList76 = soy.utils.getDataSingleAttr(data, 'files')
        fileListLen76 = len(fileList76)
        for fileIndex76, fileData76 in enumerate(fileList76):
            output += u'<tr>' + registry.lookup('muziq.fileRow')({'file': fileData76}, ijData=ijData) + u'</tr>'
        output += u'</tbody></table>' + registry.lookup('muziq.ui.pagination')(data, ijData=ijData)
    else:
        output += u'<p>You have no files in your library yet!</p>'
    return output

registry.register('muziq.filesPageContent', muziq__filesPageContent)


def muziq__uploadDialog(data=None, ijData=None):
    return u'<div class=\"modal fade\" role=\"dialog\"><div class=\"modal-dialog\"><div class=\"modal-content\"><div class=\"modal-header\"><button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button><h4 class=\"modal-title\">Upload files</h4></div><div class=\"modal-body upload\"><div class=\"upload-drop\"><div class=\"upload-info\"><div class=\"help\">Select music files to upload. You can select more than one file at a time. You can also drag and drop files anywhere on this page to start uploading.</div></div></div></div><div class=\"modal-footer\"><button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button><button type=\"button\" class=\"btn btn-primary\">Select files</button></div></div></div></div>'

registry.register('muziq.uploadDialog', muziq__uploadDialog)


def muziq__uploadPreviewsTable(data=None, ijData=None):
    return u'<table class=\"upload-previews table table-condensed\"><thead><tr><th>File</th><th>Status</th></tr></thead><tbody></tbody></table>'

registry.register('muziq.uploadPreviewsTable', muziq__uploadPreviewsTable)


def muziq__uploadPreviewRow(data=None, ijData=None):
    return u'<tr class=\"dz-preview dz-file-preview\"><td><span data-dz-name></span> (<span data-dz-size></span>)</td><td><span class=\"dz-progress\" data-dz-uploadprogress></span><span class=\"dz-success-mark\">\u2714</span><span class=\"dz-error-mark\">\u2718</span></td></tr>'

registry.register('muziq.uploadPreviewRow', muziq__uploadPreviewRow)


def muziq__fileRow(data=None, ijData=None):
    return u'<td><a href=\"javascript:void(0)\" class=\"play-btn\" data-track=\"' + soy.utils.escapeHtmlAttribute(soy.utils.getDataSingleAttr(soy.utils.getDataSingleAttr(data, 'file'), 'id')) + u'\"><i class=\"icon-play2\"></i></a></td><td>' + soy.utils.escapeHtml(soy.utils.getDataSingleAttr(soy.utils.getDataSingleAttr(data, 'file'), 'name')) + u'</td><td>' + soy.utils.escapeHtml(soy.utils.getDataSingleAttr(soy.utils.getDataSingleAttr(data, 'file'), 'uploaded')) + u'</td>'

registry.register('muziq.fileRow', muziq__fileRow)

# This file was automatically generated from index.soy.
# Please don't edit this file by hand.

import soy.utils
import soy.data
import soy.bidi
import math
import random

try:
    registry
except NameError:
    raise ImportError('This file should be imported using soy.tofu.SoyTofu')



def muziq__indexPage(data=None, ijData=None):
    data = {} if data is None else data
    return registry.lookup('muziq.appPage')({'user': soy.utils.getDataSingleAttr(data, 'user'), 'content': registry.lookup('muziq.indexPageContent')(data, ijData=ijData)}, ijData=ijData)

registry.register('muziq.indexPage', muziq__indexPage)


def muziq__indexPageContent(data=None, ijData=None):
    return u'<h1>Hello World!</h1>'

registry.register('muziq.indexPageContent', muziq__indexPageContent)

# This file was automatically generated from login.soy.
# Please don't edit this file by hand.

import soy.utils
import soy.data
import soy.bidi
import math
import random

try:
    registry
except NameError:
    raise ImportError('This file should be imported using soy.tofu.SoyTofu')



def muziq__loginPage(data=None, ijData=None):
    return registry.lookup('muziq.mainPage')({'title': u'Sign in', 'content': registry.lookup('muziq.loginPageContent')(data, ijData=ijData)}, ijData=ijData)

registry.register('muziq.loginPage', muziq__loginPage)


def muziq__loginPageContent(data=None, ijData=None):
    data = {} if data is None else data
    output = u'<div class=\"container\"><form class=\"form-login\" role=\"form\" action=\"/login\" method=\"post\"><h2 class=\"form-login-heading\">Please log in</h2><div class=\"form-group'
    if soy.utils.getDataSingleAttr(soy.utils.getDataSingleAttr(data, 'errors'), 'email'):
        output += u' has-error has-feedback'
    output += u'\"><input id=\"email\" name=\"email\" type=\"email\" class=\"form-control\" placeholder=\"Email address\" required autofocus value=\"'
    if soy.utils.getDataSingleAttr(data, 'email'):
        output += soy.utils.escapeHtmlAttribute(soy.utils.getDataSingleAttr(data, 'email'))
    output += u'\">'
    if soy.utils.getDataSingleAttr(soy.utils.getDataSingleAttr(data, 'errors'), 'email'):
        output += u'<span class=\"help-block\">'
        errorList129 = soy.utils.getDataSingleAttr(soy.utils.getDataSingleAttr(data, 'errors'), 'email')
        errorListLen129 = len(errorList129)
        for errorIndex129, errorData129 in enumerate(errorList129):
            output += soy.utils.escapeHtml(errorData129)
        output += u'</span>'
    output += u'</div><div class=\"form-group'
    if soy.utils.getDataSingleAttr(soy.utils.getDataSingleAttr(data, 'errors'), 'password'):
        output += u' has-error has-feedback'
    output += u'\"><input id=\"password\" name=\"password\" type=\"password\" class=\"form-control\" placeholder=\"Password\" required>'
    if soy.utils.getDataSingleAttr(soy.utils.getDataSingleAttr(data, 'errors'), 'password'):
        output += u'<span class=\"help-block\">'
        errorList141 = soy.utils.getDataSingleAttr(soy.utils.getDataSingleAttr(data, 'errors'), 'password')
        errorListLen141 = len(errorList141)
        for errorIndex141, errorData141 in enumerate(errorList141):
            output += soy.utils.escapeHtml(errorData141)
        output += u'</span>'
    output += u'</div><label class=\"checkbox\"><input name=\"remember_me\" type=\"checkbox\" value=\"yes\"> Remember me</label><button class=\"btn btn-primary btn-block\" type=\"submit\">Log in</button><div class=\"form-login-help pull-right\"><a href=\"/help/login\" class=\"app-link\">Need help?</a></div></form></div>'
    return output

registry.register('muziq.loginPageContent', muziq__loginPageContent)

# This file was automatically generated from music.soy.
# Please don't edit this file by hand.

import soy.utils
import soy.data
import soy.bidi
import math
import random

try:
    registry
except NameError:
    raise ImportError('This file should be imported using soy.tofu.SoyTofu')



def muziq__musicPage(data=None, ijData=None):
    data = {} if data is None else data
    return registry.lookup('muziq.appPage')({'title': u'Music', 'user': soy.utils.getDataSingleAttr(data, 'user'), 'content': registry.lookup('muziq.musicPageContent')(data, ijData=ijData)}, ijData=ijData)

registry.register('muziq.musicPage', muziq__musicPage)


def muziq__musicPageContent(data=None, ijData=None):
    return u'<h1>My Library</h1>'

registry.register('muziq.musicPageContent', muziq__musicPageContent)

# This file was automatically generated from search.soy.
# Please don't edit this file by hand.

import soy.utils
import soy.data
import soy.bidi
import math
import random

try:
    registry
except NameError:
    raise ImportError('This file should be imported using soy.tofu.SoyTofu')



def muziq__search__artistSuggestionHeader(data=None, ijData=None):
    output = u''
    if not soy.utils.getDataSingleAttr(data, 'isEmpty'):
        output += u'<strong class=\"tt-dropdown-header\">Artists</strong>'
    return output

registry.register('muziq.search.artistSuggestionHeader', muziq__search__artistSuggestionHeader)


def muziq__search__albumSuggestionHeader(data=None, ijData=None):
    output = u''
    if not soy.utils.getDataSingleAttr(data, 'isEmpty'):
        output += u'<strong class=\"tt-dropdown-header\">Albums</strong>'
    return output

registry.register('muziq.search.albumSuggestionHeader', muziq__search__albumSuggestionHeader)


def muziq__search__trackSuggestionHeader(data=None, ijData=None):
    output = u''
    if not soy.utils.getDataSingleAttr(data, 'isEmpty'):
        output += u'<strong class=\"tt-dropdown-header\">Tracks</strong>'
    return output

registry.register('muziq.search.trackSuggestionHeader', muziq__search__trackSuggestionHeader)


def muziq__search__seeAllResultsSuggestion(data=None, ijData=None):
    return u'<a href=\"/search?q=' + soy.utils.escapeUri(soy.utils.getDataSingleAttr(data, 'query')) + u'\" class=\"app-link\">See all results</a>'

registry.register('muziq.search.seeAllResultsSuggestion', muziq__search__seeAllResultsSuggestion)


def muziq__search__artistSuggestion(data=None, ijData=None):
    output = u'<a href=\"/music/artist/' + soy.utils.escapeHtmlAttribute(soy.utils.getDataSingleAttr(data, 'id')) + u'\" class=\"app-link\">' + soy.utils.escapeHtml(soy.utils.getDataSingleAttr(data, 'name'))
    if soy.utils.getDataSingleAttr(data, 'comment'):
        output += u' <small class=\"text-muted\">(' + soy.utils.escapeHtml(soy.utils.getDataSingleAttr(data, 'comment')) + u')</small>'
    output += u'</a>'
    return output

registry.register('muziq.search.artistSuggestion', muziq__search__artistSuggestion)


def muziq__search__albumSuggestion(data=None, ijData=None):
    return u'<a href=\"/music/album/' + soy.utils.escapeHtmlAttribute(soy.utils.getDataSingleAttr(data, 'id')) + u'\" class=\"app-link\">' + soy.utils.escapeHtml(soy.utils.getDataSingleAttr(data, 'name')) + u'<span class=\"text-muted\"> - ' + soy.utils.escapeHtml(soy.utils.getDataSingleAttr(data, 'artist')) + u'</span></a>'

registry.register('muziq.search.albumSuggestion', muziq__search__albumSuggestion)


def muziq__search__trackSuggestion(data=None, ijData=None):
    return u'<a href=\"/music/track/' + soy.utils.escapeHtmlAttribute(soy.utils.getDataSingleAttr(data, 'id')) + u'\" class=\"app-link\">' + soy.utils.escapeHtml(soy.utils.getDataSingleAttr(data, 'name')) + u'<span class=\"text-muted\"> - ' + soy.utils.escapeHtml(soy.utils.getDataSingleAttr(data, 'artist')) + u'</span></a>'

registry.register('muziq.search.trackSuggestion', muziq__search__trackSuggestion)

# This file was automatically generated from ui.soy.
# Please don't edit this file by hand.

import soy.utils
import soy.data
import soy.bidi
import math
import random

try:
    registry
except NameError:
    raise ImportError('This file should be imported using soy.tofu.SoyTofu')



def muziq__ui__pagination(data=None, ijData=None):
    output = u''
    if soy.utils.getDataSingleAttr(soy.utils.getDataSingleAttr(data, 'pagination'), 'pageList') and (soy.utils.getDataSingleAttr(soy.utils.getDataSingleAttr(data, 'pagination'), 'hasPrev') or soy.utils.getDataSingleAttr(soy.utils.getDataSingleAttr(data, 'pagination'), 'hasNext')):
        output += u'<ul class=\"pagination\">'
        if soy.utils.getDataSingleAttr(soy.utils.getDataSingleAttr(data, 'pagination'), 'hasPrev'):
            output += u'<li><a href=\"/files?page=' + soy.utils.escapeUri(soy.utils.getDataSingleAttr(soy.utils.getDataSingleAttr(data, 'pagination'), 'page') - 1) + u'\" class=\"app-link\">\xab</a></li>'
        else:
            output += u'<li class=\"disabled\"><span>\xab</span></li>'
        pageList212 = soy.utils.getDataSingleAttr(soy.utils.getDataSingleAttr(data, 'pagination'), 'pageList')
        pageListLen212 = len(pageList212)
        for pageIndex212, pageData212 in enumerate(pageList212):
            if pageData212:
                if pageData212 != soy.utils.getDataSingleAttr(soy.utils.getDataSingleAttr(data, 'pagination'), 'page'):
                    output += u'<li><a href=\"/files'
                    if pageData212 != 1:
                        output += u'?page=' + soy.utils.escapeUri(pageData212)
                    output += u'\" class=\"app-link\">' + soy.utils.escapeHtml(pageData212) + u'</a></li>'
                else:
                    output += u'<li class=\"active\"><span>' + soy.utils.escapeHtml(pageData212) + u'</span></li>'
            else:
                output += u'<li><span>\u2026</span></li>'
        if soy.utils.getDataSingleAttr(soy.utils.getDataSingleAttr(data, 'pagination'), 'hasNext'):
            output += u'<li><a href=\"/files?page=' + soy.utils.escapeUri(soy.utils.getDataSingleAttr(soy.utils.getDataSingleAttr(data, 'pagination'), 'page') + 1) + u'\" class=\"app-link\">\xbb</a></li>'
        else:
            output += u'<li class=\"disabled\"><span>\xbb</span></li>'
        output += u'</ul>'
    return output

registry.register('muziq.ui.pagination', muziq__ui__pagination)


def muziq__ui__searchArtistHeader(data=None, ijData=None):
    output = u''
    if not soy.utils.getDataSingleAttr(data, 'isEmpty'):
        output += u'<strong class=\"tt-dropdown-header\">Artists</strong>'
    return output

registry.register('muziq.ui.searchArtistHeader', muziq__ui__searchArtistHeader)


def muziq__ui__searchRecordingHeader(data=None, ijData=None):
    output = u''
    if not soy.utils.getDataSingleAttr(data, 'isEmpty'):
        output += u'<strong class=\"tt-dropdown-header\">Tracks</strong>'
    return output

registry.register('muziq.ui.searchRecordingHeader', muziq__ui__searchRecordingHeader)


def muziq__ui__searchReleaseGroupHeader(data=None, ijData=None):
    output = u''
    if not soy.utils.getDataSingleAttr(data, 'isEmpty'):
        output += u'<strong class=\"tt-dropdown-header\">Release group</strong>'
    return output

registry.register('muziq.ui.searchReleaseGroupHeader', muziq__ui__searchReleaseGroupHeader)


def muziq__ui__searchLabelHeader(data=None, ijData=None):
    output = u''
    if not soy.utils.getDataSingleAttr(data, 'isEmpty'):
        output += u'<strong class=\"tt-dropdown-header\">Labels</strong>'
    return output

registry.register('muziq.ui.searchLabelHeader', muziq__ui__searchLabelHeader)


def muziq__ui__searchArtistSuggestion(data=None, ijData=None):
    output = u'<a href=\"/music/artist/' + soy.utils.escapeHtmlAttribute(soy.utils.getDataSingleAttr(data, 'id')) + u'\" class=\"app-link\">' + soy.utils.escapeHtml(soy.utils.getDataSingleAttr(data, 'name'))
    if soy.utils.getDataSingleAttr(data, 'comment'):
        output += u' <small class=\"text-muted\">(' + soy.utils.escapeHtml(soy.utils.getDataSingleAttr(data, 'comment')) + u')</small>'
    output += u'</a>'
    return output

registry.register('muziq.ui.searchArtistSuggestion', muziq__ui__searchArtistSuggestion)


def muziq__ui__searchLabelSuggestion(data=None, ijData=None):
    output = u'<a href=\"/music/label/' + soy.utils.escapeHtmlAttribute(soy.utils.getDataSingleAttr(data, 'id')) + u'\" class=\"app-link\">' + soy.utils.escapeHtml(soy.utils.getDataSingleAttr(data, 'name'))
    if soy.utils.getDataSingleAttr(data, 'comment'):
        output += u' <small class=\"text-muted\">(' + soy.utils.escapeHtml(soy.utils.getDataSingleAttr(data, 'comment')) + u')</small>'
    output += u'</a>'
    return output

registry.register('muziq.ui.searchLabelSuggestion', muziq__ui__searchLabelSuggestion)


def muziq__ui__searchRecordingSuggestion(data=None, ijData=None):
    return u'<a href=\"/music/recording/' + soy.utils.escapeHtmlAttribute(soy.utils.getDataSingleAttr(data, 'id')) + u'\" class=\"app-link\">' + soy.utils.escapeHtml(soy.utils.getDataSingleAttr(data, 'name')) + u'<span class=\"text-muted\"> - ' + soy.utils.escapeHtml(soy.utils.getDataSingleAttr(data, 'artist')) + u'</span></a>'

registry.register('muziq.ui.searchRecordingSuggestion', muziq__ui__searchRecordingSuggestion)


def muziq__ui__searchShowAllResults(data=None, ijData=None):
    return u'<a href=\"/search?q=' + soy.utils.escapeUri(soy.utils.getDataSingleAttr(data, 'query')) + u'\" class=\"app-link\">Show all results</a>'

registry.register('muziq.ui.searchShowAllResults', muziq__ui__searchShowAllResults)


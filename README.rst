Muziq Website
=============

Create a Python virtualenv::

    $ virtualenv --system-site-packages e
    $ . e/bin/activate
    $ pip install -r requirements.txt

Start the development server::

    $ MUZIQ_SETTINGS=`pwd`/settings.py python manage.py runserver

Start the development server in a Vagrant box::

    $ vagrant up

Vagrant box links:

* `Solr <http://192.168.33.10:8983/solr/>`_
* `RabbitMQ <http://192.168.33.10:55672/>`_

